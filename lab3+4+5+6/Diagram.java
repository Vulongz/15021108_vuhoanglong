import sun.security.provider.SHA;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by lumosnysm on 14/07/2017.
 */
public class Diagram {

    private ArrayList<Layer> Layers = new ArrayList<>();
    private HashMap<String, Integer> map = new HashMap<>();

    void addLayer(Layer l) {
        this.Layers.add(l);
        l.getMap().forEach((k, v) -> map.merge(k, v, (v1, v2) -> v1 + v2));
    }

    void diagramDeleteCircle() {
        for (Layer l : Layers) {
            l.layerDeleteCircle();
        }
    }

    void displayComponent() {
        int i = 1;
        for (Layer l : Layers) {
            if (l.isVisible()) {
                System.out.println("Layer " + i + ":");
                l.displayComponent();
                i++;
            }
        }
    }

    void divide() {
        if (map.size() > Layers.size()) {
            for (int i = Layers.size(); i < map.size(); i++) Layers.add(new Layer());
        }
        for (int i = 0; i < Layers.size(); i++) {
            for (int j = 0; j < Layers.size(); j++) {
                if (i != j) {
                    ArrayList<Shape> temp = Layers.get(j).getAndRemove((String) map.keySet().toArray()[i]);
                    for (Shape s : temp) Layers.get(i).addShape(s);
                }
            }
        }
    }
}