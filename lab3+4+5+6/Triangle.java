import static java.lang.Math.sqrt;

/**
 * Created by lumosnysm on 14/07/2017.
 */
public class Triangle extends Shape {

    private double a, b, c;

    Triangle() {
        super();
        super.setPoints(new Point[]{new Point(0, 0), new Point(0, 1), new Point(1, 0)});
        caculate3Side();
    }

    Triangle(double x1, double y1, double x2, double y2, double x3, double y3) {
        super();
        super.setPoints(new Point[]{new Point(x1, y1), new Point(x2, y2), new Point(x3, y3)});
        caculate3Side();
    }

    Triangle(double x1, double y1, double x2, double y2, double x3, double y3, String color, boolean filled) {
        super(color, filled);
        super.setPoints(new Point[]{new Point(x1, y1), new Point(x2, y2), new Point(x3, y3)});
        caculate3Side();
    }

    private void caculate3Side() {
        this.a = getPoint(0).distance(getPoint(1));
        this.b = getPoint(0).distance(getPoint(2));
        this.c = getPoint(1).distance(getPoint(2));
    }

    double getArea() {
        double p = (a + b + c) / 2;
        return sqrt(p * (p - a) * (p - b) * (p - c));
    }

    double getPerimeter() {
        return a + b + c;
    }

    boolean isValid() {
        if ((getPoint(0).getX() - getPoint(1).getX()) / (getPoint(0).getX() - getPoint(2).getX())
                == (getPoint(0).getY() - getPoint(1).getY()) / (getPoint(0).getY() - getPoint(2).getY()))
            return false;
        else return true;
    }

    public String toString() {
        return super.toString() + ", " + this.a + ", " + this.b + ", " + this.c;
    }

}