import static java.lang.StrictMath.sqrt;

public class Hexagon extends Shape {

    private double side;

    /**
     * 3 diem tuong ung A, B, C , AB=AC, go'c A = 120.
     */

    Hexagon() {
        super();
        super.setPoints(new Point[]{new Point(1, 0), new Point(0.5, sqrt(3) / 2), new Point(0.5, -sqrt(3) / 2)});
        caculateSide();
    }

    Hexagon(double x1, double y1, double x2, double y2, double x3, double y3) {
        super();
        super.setPoints(new Point[]{new Point(x1, y1), new Point(x2, y2), new Point(x3, y3)});
        caculateSide();
    }

    Hexagon(double x1, double y1, double x2, double y2, double x3, double y3, String color, boolean filled) {
        super(color, filled);
        super.setPoints(new Point[]{new Point(x1, y1), new Point(x2, y2), new Point(x3, y3)});
        caculateSide();
    }

    private void caculateSide() {
        this.side = getPoint(0).distance(getPoint(1));
    }

    boolean isValid() {
        double v1X = getPoint(1).getX() - getPoint(0).getX();
        double v1Y = getPoint(1).getY() - getPoint(0).getY();
        double v2X = getPoint(2).getX() - getPoint(0).getX();
        double v2Y = getPoint(2).getY() - getPoint(0).getY();
        double cosA = (v1X * v2X + v1Y * v2Y) / (sqrt(v1X * v1X + v1Y * v1Y) * sqrt(v2X * v2X + v2Y * v2Y));
        return getPoint(0).distance(getPoint(1)) == getPoint(0).distance(getPoint(2))
                && cosA == -0.5;
    }

    public String toString() {
        return super.toString() + ", " + this.side;
    }

}
