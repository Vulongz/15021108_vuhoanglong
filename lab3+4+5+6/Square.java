/**
 * Created by lumosnysm on 14/07/2017.
 */
public class Square extends Rectangle {

    Square() {
        super();
    }

    Square(double x1, double y1, double x2, double y2, double x3, double y3) {
        super(x1, y1, x2, y2, x3, y3);
    }

    Square(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, String color, boolean filled) {
        super(x1, y1, x2, y2, x3, y3, color, filled);
    }

    boolean isValid() {
        return super.isValid() && (super.getWidth() == super.getLength());
    }

    double getSide() {
        return super.getWidth();
    }

    public String toString() {
        return super.toString();
    }
}