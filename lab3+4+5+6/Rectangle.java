import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by lumosnysm on 14/07/2017.
 */
public class Rectangle extends Shape {

    private double width;
    private double length;

    /**
     * 3 diem trong contructor tuong ung A,B,C AB vuong goc AC
     */

    Rectangle() {
        super();
        super.setPoints(new Point[]{new Point(0, 0), new Point(0, 1.0), new Point(1.0, 0)});
        caculateWidtAndLength();
    }

    Rectangle(double x1, double y1, double x2, double y2, double x3, double y3) {
        super();
        super.setPoints(new Point[]{new Point(x1, y1), new Point(x2, y2), new Point(x3, y3)});
        caculateWidtAndLength();
    }

    Rectangle(double x1, double y1, double x2, double y2, double x3, double y3, String color, boolean filled) {
        super(color, filled);
        super.setPoints(new Point[]{new Point(x1, y1), new Point(x2, y2), new Point(x3, y3)});
        caculateWidtAndLength();
    }

    private void caculateWidtAndLength() {
        double temp1 = getPoint(0).distance(getPoint(1));
        double temp2 = getPoint(0).distance(getPoint(2));
        if (temp1 <= temp2) {
            this.width = temp1;
            this.length = temp2;
        } else {
            this.width = temp2;
            this.length = temp1;
        }
    }

    boolean isValid() {
        double v1X = getPoint(0).getX() - getPoint(1).getX();
        double v1Y = getPoint(0).getY() - getPoint(1).getY();
        double v2X = getPoint(0).getX() - getPoint(2).getX();
        double v2Y = getPoint(0).getY() - getPoint(2).getY();

        return v1X * v2X + v1Y * v2Y == 0;
    }

    double getWidth() {
        return this.width;
    }

    double getLength() {
        return this.length;
    }

    double getArea() {
        return this.width * this.length;
    }

    double getPerimeter() {
        return this.width * 2 + this.length * 2;
    }

    public String toString() {
        return super.toString() + ", " + this.width + ", " + this.length;
    }
}