/**
 * Created by lumosnysm on 14/07/2017.
 */
public class Shape {

    private String color;
    private boolean filled;
    private Point[] points;


    Shape() {
        this.color = "red";
        this.filled = true;
    }

    Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    void setPoints(Point[] points) {
        this.points = points;
    }

    Point getPoint(int n) {
        return points[n];
    }

    private String pointsToString() {
        String s = "";
        for (Point p : points) {
            s += p.toString();
        }
        return s;
    }

    boolean equals(Shape other) {
        if (this.getClass() != other.getClass()) return false;

        for (Point otherP : other.points) {
            boolean found = false;
            for (Point currP : points) {
                if (otherP.equals(currP)) found = true;
            }
            if (!found) return false;
        }

        return true;
    }

    boolean isValid() {
        return true;
    }

    String getColor() {
        return this.color;
    }

    void setColor(String color) {
        this.color = color;
    }

    boolean isFilled() {
        return this.filled;
    }

    void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString() {

        return this.getClass().getSimpleName() + ", " + this.color + ", " + this.filled + " [" + pointsToString() + "] ";
    }
}