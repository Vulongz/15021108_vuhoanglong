import static java.lang.StrictMath.pow;
import static java.lang.StrictMath.sqrt;

/**
 * Created by lumosnysm on 18/07/2017.
 */
public class Point {

    private double x, y;

    double getY() {
        return y;
    }

    void setY(double y) {
        this.y = y;
    }

    double getX() {
        return x;
    }

    void setX(double x) {
        this.x = x;
    }

    public Point() {
        this.x = 1;
        this.y = 1;
    }

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    boolean equals(Point other) {
        return (x == other.getX() && y == other.getY());
    }

    double distance(Point other) {
        return sqrt(pow(x - other.getX(), 2) + pow(y - other.getY(), 2));
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}