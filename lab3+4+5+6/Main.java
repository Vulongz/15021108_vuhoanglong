/**
 * Created by lumosnysm on 14/07/2017.
 */
public class Main {
    public static void main(String[] args) {
        Circle c = new Circle();
        Triangle t = new Triangle();
        Rectangle r = new Rectangle();
        Square s = new Square();
        Hexagon h = new Hexagon();

        Layer l1 = new Layer();
        Layer l2 = new Layer();
        Layer l3 = new Layer();

        l1.addShape(c);
        l1.addShape(t);
        l1.addShape(r);
        l1.addShape(s);
        l1.addShape(h);

        l2.addShape(c);
        l2.addShape(t);
        l2.addShape(r);
        l2.addShape(s);
        l2.addShape(h);

        l3.addShape(c);
        l3.addShape(t);
        l3.addShape(r);
        l3.addShape(s);
        l3.addShape(h);

        Diagram D = new Diagram();
        D.addLayer(l1);
        D.addLayer(l2);
        D.addLayer(l3);


        D.divide();
        D.displayComponent();
    }
}