import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by lumosnysm on 14/07/2017.
 */
public class Layer {

    private ArrayList<Shape> shapes = new ArrayList<Shape>();
    private boolean visible = true;
    private HashMap<String, Integer> map = new HashMap<>();

    boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    void addShape(Shape s) {
        if (s.isValid()) {
            this.shapes.add(s);
            if (map.containsKey(s.getClass().getSimpleName())) {
                map.put(s.getClass().getSimpleName(), map.get(s.getClass().getSimpleName()) + 1);
            } else map.put(s.getClass().getSimpleName(), 1);
        }
    }

    HashMap<String, Integer> getMap() {
        return map;
    }

    void layerDeleteTriangle() {
        for (int i = 0; i < shapes.size(); i++) {
            if (shapes.get(i) instanceof Triangle) {
                shapes.remove(shapes.get(i));
                i--;
            }

        }
    }

    void layerDeleteCircle() {
        for (int i = 0; i < shapes.size(); i++) {
            if (shapes.get(i) instanceof Circle) {
                shapes.remove(shapes.get(i));
                i--;
            }
        }
    }

    void displayComponent() {
        for (Shape s : shapes) {
            System.out.println(s.toString());
        }
    }

    void deleteEqualShapes() {
        for (int i = 0; i < shapes.size() - 1; i++) {
            for (int j = i + 1; j < shapes.size(); j++) {
                if (shapes.get(i).equals(shapes.get(j))) {
                    shapes.remove(shapes.get(j));
                    j--;
                }
            }
        }
    }

    ArrayList<Shape> getAndRemove(String className) {
        ArrayList<Shape> temp = new ArrayList<>();
        for (int i = 0; i < shapes.size(); i++) {
            if (shapes.get(i).getClass().getSimpleName().equals(className)) {
                temp.add(shapes.get(i));
                shapes.remove(i);
                i--;
            }
        }
        return temp;
    }
}