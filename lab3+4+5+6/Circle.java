/**
 * Created by lumosnysm on 14/07/2017.
 */
public class Circle extends Shape {

    private double radius;

    Circle() {
        super();
        super.setPoints(new Point[]{new Point(1, 1)});
        this.radius = 1;
    }

    Circle(double radius, double x, double y) {
        super();
        super.setPoints(new Point[]{new Point(x, y)});
        this.radius = radius;
    }

    Circle(double radius, double x, double y, String color, boolean filled) {
        super(color, filled);
        super.setPoints(new Point[]{new Point(x, y)});
        this.radius = radius;
    }

    public double getArea() {
        return 3.14 * this.radius * this.radius;
    }

    public double getPerimeter() {
        return 3.14 * this.radius * 2;
    }

    public String toString() {
        return super.toString() + ", " + this.radius;
    }
}