public class StudentManagement {

    public static boolean sameGroup(Student s1, Student s2) {
        String group1 = s1.getGroup();
        String group2 = s2.getGroup();
        return group1.equals(group2);
    }
    public static void main(String[] args) {
        Student me = new Student();
        Student s1 = new Student(me);
        Student s2 = new Student("Student2", "12345678", "student3@vnu.edu.vn");
        me.setName("Vu Hoang Long");
        me.setId("15021108");
        me.setGroup("K60CAC");
        me.setEmail("lumosnysm@gmail.com");
        me.getName();
        me.getInfo();
        s2.setGroup("K59CLC");
        s1.getInfo();
        s2.getInfo();
        System.out.println(sameGroup(s1, s2));
    }

}