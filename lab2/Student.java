public class Student {

    private String name;
    private String email;
    private String group;
    private String id;

    public Student() {
        this.name = "Student";
        this.id = "000";
        this.group = "K59CB";
        this.email = "uet@vnu.edu.vn";
    }
    public Student(Student s) {
        this.name = s.name;
        this.id = s.id;
        this.group = s.group;
        this.email = s.email;
    }
    public Student(String name, String id,String email) {
        this.name = name;
        this.id = id;
        this.group = "K59CB";
        this.email = email;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return this.email;
    }
    public void setGroup(String group) {
        this.group = group;
    }
    public String getGroup() {
        return this.group;
    }
    public String getInfo() {
         String info = this.name + ", " + this.id + ", " + this.group + ", " + this.email;
        System.out.println(info);
        return info;
    }

}