import java.util.Random;

public class Question {

    private String content;
    private String[] answer = new String[4];
    private String correctAnswer;

    Question(String content, String ans1, String ans2, String ans3, String ans4) {
        this.content = content;
        this.answer[0] = ans1;
        this.answer[1] = ans2;
        this.answer[2] = ans3;
        this.answer[3] = ans4;
        this.correctAnswer = ans1;
    }

    String getContent() {
        return content;
    }

    String[] getAns() {
        return answer;
    }

    String getCorrectAnswer() {
        return correctAnswer;
    }

    void shuffleAnswers() {
        for (int i = 0; i < 10; i++) {
            Random rd = new Random();
            int x = rd.nextInt(3);
            int y = x + 1;
            if (x == 3) y = 0;
            String temp = this.answer[x];
            this.answer[x] = this.answer[y];
            this.answer[y] = temp;
        }
    }
}
