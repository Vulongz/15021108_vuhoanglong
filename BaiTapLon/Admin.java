import oracle.jrockit.jfr.JFR;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Created by lumosnysm on 23/07/2017.
 */
public class Admin extends JFrame implements ActionListener {
    private JPanel Admin;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JButton OKButton;
    private JButton playButton;

    Admin() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(400,400);
        setResizable(false);
        setContentPane(Admin);
        OKButton.addActionListener(this);
        playButton.addActionListener(this);
        //pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == OKButton) {
            BufferedWriter br = null;
            try {
                File file = new File("data.txt");
                FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
                br = new BufferedWriter(fw);
                br.newLine();
                br.write(textField1.getText());
                br.newLine();
                br.write(textField2.getText());
                br.newLine();
                br.write(textField3.getText());
                br.newLine();
                br.write(textField4.getText());
                br.newLine();
                br.write(textField5.getText());
                br.close();
            } catch (java.io.IOException e1) {
                e1.printStackTrace();
            }
            this.dispose();
            new Admin();
        }
        if (e.getSource() == playButton) {
            this.dispose();
            new Start();
        }
    }
}

