import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;

public class Start extends JFrame implements ActionListener {
    private JPanel Start;
    private JTextArea textArea1;
    private JButton buttonA;
    private JButton buttonB;
    private JButton buttonC;
    private JButton buttonD;
    private JButton a5050Button;
    private JButton endGameButton;
    private JLabel label1;
    private JLabel highScorelb;
    private JButton askButton;

    private Assistant myAs;
    private Question currentQuestion;
    private int use5050 = 0;
    private int useAsk = 0;

    Start() {
        myAs = new Assistant();
        try {
            myAs.getAllQuestions();
        } catch (IOException e) {
            e.printStackTrace();
        }
        myAs.shuffleQuestions();
        setUpQuestion();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("Play");
        setSize(300, 300);
        setContentPane(Start);
        buttonA.addActionListener(this);
        buttonB.addActionListener(this);
        buttonC.addActionListener(this);
        buttonD.addActionListener(this);
        a5050Button.addActionListener(this);
        askButton.addActionListener(this);
        endGameButton.addActionListener(this);
        buttonA.setPreferredSize(new Dimension(200, 20));
        buttonB.setPreferredSize(new Dimension(200, 20));
        buttonC.setPreferredSize(new Dimension(200, 20));
        buttonD.setPreferredSize(new Dimension(200, 20));
        //pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }

    private void setUpQuestion() {
        currentQuestion = myAs.getTheCurrentQuestion();
        currentQuestion.shuffleAnswers();
        label1.setText("Score: " + myAs.getScore());
        try {
            if (myAs.getScore() > myAs.getHighScore()) {
                highScorelb.setText("High Score: " + myAs.getScore());
            } else
                highScorelb.setText("High Score: " + myAs.getHighScore());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        textArea1.setText(currentQuestion.getContent());
        buttonA.setText("A. " + currentQuestion.getAns()[0]);
        buttonB.setText("B. " + currentQuestion.getAns()[1]);
        buttonC.setText("C. " + currentQuestion.getAns()[2]);
        buttonD.setText("D. " + currentQuestion.getAns()[3]);
    }

    private void assistantDoTheWork(String answer) {
        if (myAs.check(answer)) {
            myAs.scoreUp();
            if (myAs.getScore() == myAs.getNumberQuestions()) {
                this.dispose();
                new End(myAs.getScore(), true);
            } else {
                setUpQuestion();
                revalidate();
                repaint();
            }
        } else {
            this.dispose();
            try {
                if (myAs.getScore() > myAs.getHighScore()) {
                    myAs.saveHighScore(myAs.getScore());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            new End(myAs.getScore(), false);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == buttonA) {
            assistantDoTheWork(currentQuestion.getAns()[0]);
        }
        if (e.getSource() == buttonB) {
            assistantDoTheWork(currentQuestion.getAns()[1]);
        }
        if (e.getSource() == buttonC) {
            assistantDoTheWork(currentQuestion.getAns()[2]);
        }
        if (e.getSource() == buttonD) {
            assistantDoTheWork(currentQuestion.getAns()[3]);
        }
        if (e.getSource() == endGameButton) {
            this.dispose();
            try {
                if (myAs.getScore() > myAs.getHighScore()) {
                    myAs.saveHighScore(myAs.getScore());
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            new End(myAs.getScore(), false);
        }
        if (e.getSource() == a5050Button) {
            use5050++;
            if (use5050 > 2)
                JOptionPane.showMessageDialog(null, "You cannot use 50/50 any more", "", JOptionPane.WARNING_MESSAGE);
            else {
                int count = 0;
                Random r = new Random();
                int mem = -1;
                while (count <= 2) {
                    int temp = r.nextInt(3);
                    if (!myAs.check(currentQuestion.getAns()[temp]) && temp != mem) {
                        count++;
                        mem = temp;
                        if (temp == 0) buttonA.setText("");
                        else if (temp == 1) buttonB.setText("");
                        else if (temp == 2) buttonC.setText("");
                        else buttonD.setText("");
                    }
                }
            }
        }
        if (e.getSource() == askButton) {
            useAsk++;
            if (useAsk > 2) JOptionPane.showMessageDialog(null, "You cannot use Ask any more", "", JOptionPane.WARNING_MESSAGE);
            else {
                new AskingSupport(currentQuestion);
            }
        }
    }

}
